//
//  ViewController.swift
//  Clima2
//
//  Created by Siok on 4/17/17.
//  Copyright © 2017 Siok. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
//
    
    
    @IBOutlet weak var labelCiudad: UILabel!
    @IBOutlet weak var labelTemp: UILabel!
    @IBOutlet weak var labelSol: UILabel!
    var ciudades : Array<Array<String>> = Array<Array<String>>()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        ciudades.append(["Mexico","116545"])
        ciudades.append(["Cancun","114632"])
        ciudades.append(["La Barca","126425"])
        ciudades.append(["Chicago","2379574"])
        ciudades.append(["New Jersey","2347589"])
        ciudades.append(["Las Vegas","2436704"])
        ciudades.append(["Salta","466864"])
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.ciudades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.ciudades[row][0]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        print("Seleccionado \(self.ciudades[row][0])")
        /*let baseQuery = "https://query.yahooapis.com/v1/public/yql?format=json&q=SELECT%20*%20FROM%20weather.forecast%20WHERE%20u%20=%20%27c%27%20and%20location%20=%20%27"
        let url = NSURL(string : baseQuery + self.ciudades[row][1] + "%27")
        */
        let baseQuery = "https://query.yahooapis.com/v1/public/yql?format=json&q=select%20*%20from%20weather.forecast%20where%20u%20=%20%27c%27%20and%20woeid%20="
        let url = NSURL(string : baseQuery + self.ciudades[row][1])
        
        //print(url!)
        let datos = NSData(contentsOf: url! as URL)
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: datos! as Data, options:JSONSerialization.ReadingOptions.mutableLeaves)
            
            guard let dictionary1 = jsonObject as? Dictionary<String, Any> else {
                print("Not a Dictionary")
                // put in function
                return
            }
            //print("JSON Dictionary! \(dictionary)")
            let dictionary2 = dictionary1["query"] as! NSDictionary
            let dictionary3 = dictionary2["results"] as! NSDictionary
            let dictionaryChannel = dictionary3["channel"] as! NSDictionary
            let dictionaryLocation = dictionaryChannel["location"] as! NSDictionary
            let ciudad = dictionaryLocation["city"] as! NSString as String
            let country = dictionaryLocation["country"] as! NSString as String
            let dictionaryAstronomy = dictionaryChannel["astronomy"] as! NSDictionary
            let sunrise = dictionaryAstronomy["sunrise"] as! NSString as String
            let sunset = dictionaryAstronomy["sunset"] as! NSString as String
            let dictionaryItem = dictionaryChannel["item"] as! NSDictionary
            let dictionaryCondition = dictionaryItem["condition"] as! NSDictionary
            let temp = dictionaryCondition["temp"] as! NSString as String
            let text = dictionaryCondition["text"] as! NSString as String
            
            //item
            
            labelCiudad.text = ("\(ciudad), \(country)")
            labelTemp.text = ("\(temp) ºC, \(text)")
            labelSol.text = ("Sunrise \(sunrise), Sunset \(sunset)")
            
            //print("JSON Dictionary3! \(dictionaryLocation)")
            
            
        }
        catch let error as NSError {
            print("Found an error - \(error)")
        }
        
        
    }
}

